public class StringIntegerNode {
	
	 int amount;
	 String name;
	 StringIntegerNode next; 
	   
	 StringIntegerNode (StringIntegerNode next, String name, int amount) // Constructor
	 {
		 	this.name = name;
	        this.amount = amount;
	        this.next = next;
	 } 
	 
	 static StringIntegerNode addToFront (StringIntegerNode head, String name, int data)
	 {
	        return new StringIntegerNode(head, name, data);
	 } 
	 
	 // Converts linked list items to a string
	 static String toString (StringIntegerNode head)
	 {
		 	if (head == null)
		 	{
		 		return "Nothing";
		 	}
		 	
	        String copy = "";
	       
	        for (StringIntegerNode ptr = head; ptr != null; ptr = ptr.next)
	        {
	            copy = copy + ptr.name + " rounds - " + ptr.amount + ",";
	        }
	        
	        return copy;
	 } 
	 
	// Deletes a linked list
	 static StringIntegerNode delete (StringIntegerNode head)
	 {
		 head = null;
		 return head;
	 }
	 
	 // Getter method for size of LL
	 static int getSize (StringIntegerNode head)
	 {
		 int size = 0;
		 
		 for (StringIntegerNode ptr = head; ptr != null; ptr = ptr.next)
		 {
			 size++;
		 }
		 
		 return size;
	 }
}
