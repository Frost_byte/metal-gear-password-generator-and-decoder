import java.awt.*;
import java.io.File;
import java.io.IOException;
import javax.swing.*;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
public class print extends JFrame {
	
	// Master panel
	private JPanel master = new JPanel();
	
	// Scroll
	private JScrollPane scroll;
	
	// Lists to display
	private JPanel bosses = new JPanel();;
	private JPanel weapons = new JPanel();
	private JPanel equipment = new JPanel();
	private JPanel misc = new JPanel();
	private JPanel prisoners = new JPanel();
	private JPanel ammo = new JPanel();
	private JPanel errors = new JPanel(); 
	
	// Constructor for new window
	public print() {
		
		// Set up title
		super("Decoded Password Information");
		
		// Set up layout (one row with N columns)
		master.setLayout(new GridLayout(1,0));
		
		// Set up the font
		try {

			Font.createFont(Font.TRUETYPE_FONT, new File("src/Metal Gear.ttf")).deriveFont(12f);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

			// register the font
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("src/Metal Gear.ttf")));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FontFormatException e) {
			e.printStackTrace();
		}

		Font myFont = new Font("Metal Gear", Font.PLAIN, 16);
		Font titleFont = new Font("Metal Gear", Font.PLAIN, 20);
		ImageIcon img = new ImageIcon(
				"C:/Users/Thomas/workspace/Metal Gear Password Generator/src/res/FOXHOUND_logo.png");
		this.setIconImage(img.getImage());

		
		// Add generated lists to the window
		bosses = create_bosses(myFont, titleFont, Driver.bosses_head); 
		weapons = create_weapons(myFont, titleFont, Driver.weapons_head);
		equipment = create_equipment(myFont, titleFont, Driver.equipment_head);
		misc = create_misc(myFont, titleFont, Driver.misc_head);
		prisoners = create_prisoners(myFont, titleFont, Driver.prisoners_head);
		ammo = create_ammo(myFont, titleFont, Driver.ammo_head);
		errors = create_errors(myFont, titleFont, Driver.errors);
		
		// Add each individual panel to master panel
		master.add(bosses);
		master.add(weapons);
		master.add(equipment);
		master.add(misc);
		master.add(prisoners);
		master.add(ammo);
		master.add(errors);
		
		// Set up scroll and add it
		scroll = new JScrollPane();
		scroll.setViewportView(master);
		this.getContentPane().add(scroll, BorderLayout.CENTER);

		// Final touches
		this.setSize(new Dimension(700, 700));
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	// Panel creation methods
	public JPanel create_errors (Font myFont, Font titleFont, StringIntegerNode head)
	{
		DefaultListModel<String> model = new DefaultListModel<String>();
		String[] array = StringIntegerNode.toString(head).split(",+");
		
		for (int counter = 0; counter < array.length; counter++)
		{
			model.addElement(array[counter]);
		}
		
		JList<String> copy = new JList<String>(model);
		copy.setFont(myFont);
		copy.setVisibleRowCount(5);
		copy.setForeground(Color.BLACK);

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(copy);
		panel.setBorder(BorderFactory.createTitledBorder(panel.getBorder(), "Errors", TitledBorder.CENTER, TitledBorder.CENTER
				, titleFont, Color.WHITE));
		return panel; 
	} 
	
	public JPanel create_ammo (Font myFont, Font titleFont, StringIntegerNode head)
	{
		DefaultListModel<String> model = new DefaultListModel<String>();
		String[] array = StringIntegerNode.toString(head).split(",+");
		
		for (int counter = 0; counter < array.length; counter++)
		{
			model.addElement(array[counter]);
		}
		
		JList<String> copy = new JList<String>(model);
		copy.setFont(myFont);
		copy.setVisibleRowCount(5);
		copy.setForeground(Color.ORANGE);

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(copy);
		panel.setBorder(BorderFactory.createTitledBorder(panel.getBorder(), "Ammo", TitledBorder.CENTER, TitledBorder.CENTER
				, titleFont, Color.ORANGE));
		return panel; 
	} 
	
	public JPanel create_misc (Font myFont, Font titleFont, StringNode head)
	{
		DefaultListModel<String> model = new DefaultListModel<String>();
		String[] array = StringNode.toString(head).split(",+");
		
		for (int counter = 0; counter < array.length; counter++)
		{
			model.addElement(array[counter]);
		}
		
		// Add Snake's Rank manually
		model.addElement("Rank is " + Driver.snake_rank);
		model.addElement("Original password is " + Driver.original_password);
		model.addElement("Corrected password is " + Driver.corrected);
		
		JList<String> copy = new JList<String>(model);
		copy.setFont(myFont);
		copy.setVisibleRowCount(5);
		copy.setForeground(Color.MAGENTA);

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(copy);
		panel.setBorder(BorderFactory.createTitledBorder(panel.getBorder(), "Miscellaneous", TitledBorder.CENTER, TitledBorder.CENTER
				, titleFont, Color.MAGENTA));
		return panel;
	} 
	
	public JPanel create_prisoners (Font myFont, Font titleFont, StringNode head)
	{
		DefaultListModel<String> model = new DefaultListModel<String>();
		String[] array = StringNode.toString(head).split(",+");
		
		for (int counter = 0; counter < array.length; counter++)
		{
			model.addElement(array[counter]);
		}
		
		JList<String> copy = new JList<String>(model);
		copy.setFont(myFont);
		copy.setVisibleRowCount(5);
		copy.setForeground(Color.BLUE);

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(copy);
		panel.setBorder(BorderFactory.createTitledBorder(panel.getBorder(), "Hostages", TitledBorder.CENTER, TitledBorder.CENTER
				, titleFont, Color.BLUE));
		return panel;
	}
	
	public JPanel create_equipment (Font myFont, Font titleFont, StringNode head)
	{
		DefaultListModel<String> model = new DefaultListModel<String>();
		String[] array = StringNode.toString(head).split(",+");
		
		for (int counter = 0; counter < array.length; counter++)
		{
			model.addElement(array[counter]);
		}
		
		JList<String> copy = new JList<String>(model);
		copy.setFont(myFont);
		copy.setVisibleRowCount(5);
		copy.setForeground(Color.GREEN);
		
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(copy);
		panel.setBorder(BorderFactory.createTitledBorder(panel.getBorder(), "Equipment", TitledBorder.CENTER, TitledBorder.CENTER
				, titleFont, Color.GREEN));
		return panel;
	}
	
	public JPanel create_weapons (Font myFont, Font titleFont, StringNode head)
	{
		DefaultListModel<String> model = new DefaultListModel<String>();
		String[] array = StringNode.toString(head).split(",+");
		
		for (int counter = 0; counter < array.length; counter++)
		{
			model.addElement(array[counter]);
		}
		
		JList<String> copy = new JList<String>(model);
		copy.setFont(myFont);
		copy.setVisibleRowCount(5);
		copy.setForeground(Color.CYAN);
		
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(copy);
		panel.setBorder(BorderFactory.createTitledBorder(panel.getBorder(), "Weapons", TitledBorder.CENTER, TitledBorder.CENTER
				, titleFont, Color.CYAN));
		return panel;
	}
	
	public JPanel create_bosses (Font myFont, Font titleFont, StringNode boss_head)
	{
		DefaultListModel<String> model = new DefaultListModel<String>();
		String[] boss_array = StringNode.toString(boss_head).split(",+");
		
		for (int counter = 0; counter < boss_array.length; counter++)
		{
			model.addElement(boss_array[counter]);
		}
		
		JList<String> copy = new JList<String>(model);
		copy.setFont(myFont);
		copy.setVisibleRowCount(5);
		copy.setForeground(Color.RED);
		
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(copy);
		panel.setBorder(BorderFactory.createTitledBorder(panel.getBorder(), "Bosses", TitledBorder.CENTER, TitledBorder.CENTER
				, titleFont, Color.RED));
		return panel;
	} 

}
