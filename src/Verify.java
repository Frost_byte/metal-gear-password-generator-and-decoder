public class Verify {
	
	static String correct;
		// Returns the correct digits
		static char get_digit_key (int offset)
		{
			switch (offset)
			{
			case 0: return '1';
			case 1: return '2';
			case 2: return '3';
			case 3: return '4';
			case 4: return '5';
			case 5: return '6';
			case 6: return 'A';
			case 7: return 'B';
			case 8: return 'C';
			case 9: return 'D';
			case 10: return 'E';
			case 11: return 'F';
			case 12: return 'G';
			case 13: return 'H';
			case 14: return 'I';
			case 15: return 'J';
			case 16: return 'K';
			case 17: return 'L';
			case 18: return 'M';
			case 19: return 'N';
			case 20: return 'O';
			case 21: return 'P';
			case 22: return 'Q';
			case 23: return 'R';
			case 24: return 'S';
			case 25: return 'T';
			case 26: return 'U';
			case 27: return 'V';
			case 28: return 'W';
			case 29: return 'X';
			case 30: return 'Y';
			case 31: return 'Z';
			case 32: return '1';
			default: return '\0';
			}
		} 
		
		// Calculate the sum required for determining the checksum
		static void calculate_sum (String copy, char checksum)
		{
			int total = 0;
			int value;
			String password = "";
			
			for (int counter = 0; counter < copy.length(); counter = counter + 5)
			{
				value = Integer.parseInt(copy.substring(counter, counter + 5), 2);
				total = total + value;	
				password = password + get_digit_key(value);
				
				// Add spacing to make password easier to read
				if (password.length() == 5)
				{
					password = password + " ";
				}
				
				else if (password.length() == 11)
				{
					password = password + " ";
				}
				
				else if (password.length() == 17)
				{
					password = password + " ";
				}
				
				else if (password.length() == 23) 
				{
					password = password + " ";
				} 
			}
			
			
			checksum_verification(password,checksum,total);
		}
		
		// Verify the checksum
		static void checksum_verification (String password, char checksum, int sum)
		{
			if (sum > 507)
			{
				sum = sum + 2;
			}
			
			else if (sum > 252 && sum < 507)
			{
				sum++;
			}
			
			sum = sum + 7;
			
			while (sum > 32)
			{
				sum = sum - 32;
			} 
			
			
			char actual_checksum = get_digit_key(sum); 
			
			if (actual_checksum != Character.toUpperCase(checksum))
			{
				Driver.errors = StringIntegerNode.addToFront(Driver.errors, "Checksum failed", sum); 
				correct =  password + actual_checksum;
			}
			
			correct =  password + actual_checksum;
		}

}
