public class weaponsLL {
	
	static StringNode head;
	
	public weaponsLL()
	{
		head = null;
	}
	
	public static StringNode determine_weapons (String bits)
	{
		new weaponsLL();
		char [ ] array = bits.toCharArray();
		
		
		
		if (array[10] == '1')
		{
			head = StringNode.addToFront(head, "Ingram MAC-11");
		}
		
		if (array[11] == '1')
		{
			head = StringNode.addToFront(head, "Remote-Controlled missile launcher");
		}
		
		if (array[12] == '1')
		{
			head = StringNode.addToFront(head, "C4 Plastic explosives");
		}
		
		if (array[13] == '1')
		{
			head = StringNode.addToFront(head, "Land mine");
		}
		
		if (array[14] == '1')
		{
			head = StringNode.addToFront(head, "Beretta M92F");
		}
		
		if (array[97] == '1')
		{
			head = StringNode.addToFront(head, "Silencer");
		}
		
		if (array[98] == '1')
		{
			head = StringNode.addToFront(head, "RPG-7V Rocket Launcher");
		}
		
		if (array[99] == '1')
		{
			head = StringNode.addToFront(head, "M79 Grenade Launcher");
		}
		
		return head;
	}

}
