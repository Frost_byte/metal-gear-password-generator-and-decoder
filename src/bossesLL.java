public class bossesLL {
	
	// Singly linked-list and an integer to keep track of bosses Snake defeated and his rank respectively
	static StringNode head;
	static int rank;
	static int copy;
	
	public bossesLL() // Constructor for the bosses linked list, initializes head to null
	{
		head = null;
	}
	
	// Returns the bit-pattern corresponding to each letter
	static String get_bit_pattern (char letter)
	{
		switch (letter)
		{
				case '1': return "00000";
				case '2': return "00001";
				case '3': return "00010";
				case '4': return "00011";
				case '5': return "00100";
				case '6': return "00101";
				case 'A': return "00110";
				case 'B': return "00111";
				case 'C': return "01000";
				case 'D': return "01001";
				case 'E': return "01010";
				case 'F': return "01011";
				case 'G': return "01100";
				case 'H': return "01101";
				case 'I': return "01110";
				case 'J': return "01111";
				case 'K': return "10000";
				case 'L': return "10001";
				case 'M': return "10010";
				case 'N': return "10011";
				case 'O': return "10100";
				case 'P': return "10101";
				case 'Q': return "10110";
				case 'R': return "10111";
				case 'S': return "11000";
				case 'T': return "11001";
				case 'U': return "11010";
				case 'V': return "11011";
				case 'W': return "11100";
				case 'X': return "11101";
				case 'Y': return "11110";
				case 'Z': return "11111";
				default: return "NULL";
		}
	} 
	
	// Converts password to bits and returns the new string
	static String convert_to_bits (String password)
	{
		char [ ] upper_check = password.toCharArray();
		
		// First convert all letters to uppercase
		for (int counter = 0; counter < password.length(); counter++)
		{
				if (Character.isLetter(upper_check[counter]) && Character.isLowerCase(upper_check[counter]))
				{
					char ch = password.charAt(counter);
					ch = Character.toUpperCase(ch);
					password = password.substring(0, counter) + ch + password.substring(counter+1);
				} 
		}
		
		// Remove the spacing 
		String [] copy = password.split(" +");
		String formatted = copy[0] + copy[1] + copy[2] + copy[3] + copy[4];
		
		// Convert to bits
		String password_bits = "";
		for (int counter = 0; counter < formatted.length(); counter++)
		{
			password_bits = password_bits + get_bit_pattern(formatted.charAt(counter));
		} 
		
		return password_bits;
		
	}
	
	// Determine Snake's rank and returns the password bits
	static String determine_rank (String password_bits)
	{
		
		String rank_string = password_bits.substring(2,5);
		int value = Integer.parseInt(rank_string,2);
		
		if (value > 4 || value == 0) // Snake can only have a rank between 1 and 4 inclusively
		{
			Driver.errors = StringIntegerNode.addToFront(Driver.errors, "Rank", Integer.parseInt(password_bits.substring(2,5),2));
			rank_string = "100"; // "Correct" Snake's rank by setting it to 4 to make the player's life a little easier
			copy = 4;
			password_bits = password_bits.substring(0,2) + rank_string + password_bits.substring(5);
			return password_bits;
		}
		
		rank = Integer.parseInt(password_bits.substring(2,5),2);
		copy = rank;
		return password_bits;
		
	}
	
	/* Determine which bosses Snake has defeated by iterating through password_bits string and creating
	 * new StringNodes when necessary
	 */
	static StringNode determine_boss (String password_bits)
	{
		new bossesLL();

		char [ ] array = password_bits.toCharArray();
		
		if (array[0] == '1')
		{
			head = StringNode.addToFront(head, "Vermon CaTaffy");
		}
		
		if (array[1] == '1')
		{
			head = StringNode.addToFront(head, "Super Computer");
		}
		
		if (array[5] == '1')
		{
			head = StringNode.addToFront(head, "Tank");
		}
		
		if (array[6] == '1')
		{
			head = StringNode.addToFront(head, "Bull Tank");
		}
		
		if (array[7] == '1')
		{
			head = StringNode.addToFront(head, "Shotgunner");
		}
		
		if (array[8] == '1')
		{
			head = StringNode.addToFront(head, "Twin Shot");
		}
		
		if (array[9] == '1')
		{
			head = StringNode.addToFront(head, "Machine Gun Kid");
		} 
		
		if (array[92] == '1')
		{
			head = StringNode.addToFront(head, "Coward Duck");
		} 
		
		if (array[93] == '1')
		{
			head = StringNode.addToFront(head, "Fire Trooper");
		} 
		
		if (array[94] == '1')
		{
			head = StringNode.addToFront(head, "Arnold"); 
		} 
		
		return head;
	}

}
