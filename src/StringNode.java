public class StringNode {
	
	 public String word; 
	 public StringNode next; 
	 
	 StringNode (String word, StringNode next) // Constructor for a node
	 {
	        this.word = word;
	        this.next = next;
	 } 
	 
	 // Converts linked list items to a string
	 static String toString (StringNode head)
	 {
		 	if (head == null)
		 	{
		 		return "Nothing";
		 	}
		 	
	        String copy = "";
	       
	        for (StringNode ptr = head; ptr != null; ptr = ptr.next)
	        {
	            copy = copy + ptr.word + ",";
	        }
	        
	        return copy;
	 }
	 
	// Adds a new node to the front of the linked list for O(1) time and returns the head of the linked list
	static StringNode addToFront (StringNode head, String word)
	{
		    head = new StringNode(word, head);
		    return head;
	} 
	
	// Deletes a linked list
	static StringNode delete (StringNode head)
	{
		head = null;
		return head;
	}
	
	// Getter method for size
	static int getSize (StringNode head)
	{
		int size = 0;
		
		for (StringNode ptr = head; ptr != null; ptr = ptr.next)
		{
			size++;
		}
		
		return size;
	}
	
}
