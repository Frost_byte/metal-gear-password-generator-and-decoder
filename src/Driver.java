import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.text.MaskFormatter;
import org.apache.commons.lang3.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.awt.event.ActionEvent; 


@SuppressWarnings("serial")
public class Driver extends JFrame {
	
	// Bit-strings (each 5 bits in size)
	
	/* By default, the least significant bit in byte_0 will always be 1 because Snake's rank
	 * is always 1 at the beginning of the game
	 */
	private String byte_0 = "00001"; 
	private String byte_1 = "00000";
	private String byte_2 = "00000";
	private String byte_3 = "00000";
	private String byte_4 = "00000";
	private String byte_5 = "00000";
	private String byte_6 = "00000";
	private String byte_7 = "00000";
	private String byte_8 = "00000";
	private String byte_9 = "00000";
	private String byte_10 = "00000";
	private String byte_11 = "00000";
	private String byte_12 = "00000";
	private String byte_13 = "00000";
	private String byte_14 = "00000";
	private String byte_15 = "00000";
	private String byte_16 = "00000";
	private String byte_17 = "00000";
	private String byte_18 = "00000";
	private String byte_19 = "00000";
	private String byte_20 = "00000";
	private String byte_21 = "00000";
	private String byte_22 = "00000";
	private String byte_23 = "00000"; 

	// JScrollPane object
	private JScrollPane scroll;
	
	// Radio buttons for the ranks (stored in array), along with panel for storage 
	private JRadioButton rank[ ] = new JRadioButton[4];
	private JPanel rank_panel = new JPanel();
	
	// Checkboxes and a panel for the in-game bosses
	private JCheckBox boss[ ] = new JCheckBox[10];
	private JPanel boss_panel = new JPanel();
	
	// Checkboxes and a panel for the in-game prisoners
	private JCheckBox prisoners[ ] = new JCheckBox[22];
	private JPanel prisoners_panel = new JPanel();
	
	// Checkboxes and a panel for weapons and misc events
	private JCheckBox weapons[ ] = new JCheckBox[8];
	private JPanel weapons_panel = new JPanel();
	private JCheckBox misc[ ] = new JCheckBox[2];
	private JPanel misc_panel = new JPanel();
	
	
	// Textfields for ammunition and rations
	private JPanel ammo_rations_panel = new JPanel();
	private JPanel generate_panel = new JPanel();
	private JPanel ammo_rations_generate = new JPanel();
	private JTextField beretta_TF = new JTextField("255");
	private JTextField mine_TF = new JTextField("20");
	private JTextField c4_TF = new JTextField("20");
	private JTextField missiles_TF = new JTextField("20");
	private JTextField ingram_TF = new JTextField("255");
	private JTextField m79_TF = new JTextField("90");
	private JTextField rpg_TF = new JTextField("30");
	private JTextField rations_TF = new JTextField("15");
	
	// Buttons for ammunition and rations
	private JButton generate_password = new JButton("Generate Password");
	private JButton set_beretta = new JButton("Set Beretta M92F rounds");
	private JButton set_mine = new JButton("Set Land Mines");
	private JButton set_c4 = new JButton("Set C4 Plastic Explosives");
	private JButton set_missiles = new JButton("Set RC missiles");
	private JButton set_ingram = new JButton("Set MAC-11 rounds");
	private JButton set_m79 = new JButton("Set grenades");
	private JButton set_rpg = new JButton("Set RPGs");
	private JButton set_rations = new JButton("Set rations");
	
	// Checkboxes and a panel for equipment
	private JCheckBox equipment[ ] = new JCheckBox[24];
	private JPanel equipment_panel = new JPanel();
	
	
	// Split pane and split panel that separate row 1 and row 2
	private JSplitPane row1 = new JSplitPane();
	private JPanel weapons_misc_panel = new JPanel();
	private JPanel rank_prisoner_boss_equipment = new JPanel();
	
	// Split pane that separates row 2 and row 3 along with row 3 and row 4
	private JSplitPane row2 = new JSplitPane();
	

	// A constant panel, a panel for encoding, and a panel for decoding along with buttons for switching
	private JPanel constant = new JPanel();
	private JPanel master_panel = new JPanel();
	
	private JPanel decode_panel = new JPanel();
	private JButton decode_button = new JButton("Decode");
	private JFormattedTextField decode_TF;
	
	private CardLayout card = new CardLayout();
	
	// Menu items
	private JMenuBar menu_bar;
	private JMenu help;
	private JMenuItem about;
	private JMenu change;
	private JMenuItem button_1;
	private JMenuItem button_2;
	
	// Linked list heads, Snake's rank, and copy of original password
	static StringNode bosses_head;
	static StringNode weapons_head;
	static StringNode equipment_head;
	static StringNode misc_head;
	static StringNode prisoners_head;
	static StringIntegerNode ammo_head;
	static StringIntegerNode errors;
	static int snake_rank;
	static String original_password;
	static String corrected;
	
	
	public Driver()
	{	
		// Set up title
		super("Metal Gear Password Generator");
		
		// Set up the layout for the "constant" panel
		constant.setLayout(card);
		
		// Initialize the errors linked list to null
		errors = null;
		
		// Set up the font
		try {
		    
		    Font.createFont(Font.TRUETYPE_FONT, new File("src/Metal Gear.ttf")).deriveFont(12f);
		    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		    
		    // register the font
		    ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("src/Metal Gear.ttf")));
		} catch (IOException e) {
		    e.printStackTrace();
		} catch(FontFormatException e) {
		    e.printStackTrace(); 
		}
		
		Font myFont = new Font("Metal Gear", Font.PLAIN, 16); 
		Font titleFont = new Font("Metal Gear", Font.PLAIN, 20);
		ImageIcon img = new ImageIcon("C:/Users/Thomas/workspace/Metal Gear Password Generator/src/res/FOXHOUND_logo.png"); 
		this.setIconImage(img.getImage()); 
		
		
		ActionListener actionListener = new ActionHandler(); 
		master_panel.setLayout(new GridLayout(2,1));
		
		// Create rank, boss, hostages, weapons 
		rank_panel = create_rank(myFont, titleFont);
		boss_panel = create_boss(myFont, titleFont, actionListener);
		prisoners_panel = create_prisoners(myFont, titleFont, actionListener); 
		weapons_panel = create_weapons(myFont, titleFont, actionListener);
		misc_panel = create_misc(myFont, titleFont, actionListener);
		equipment_panel = create_equipment(myFont, titleFont, actionListener);
		
		// Put the rank, boss, prisoners, and equipment panels into one large panel
		rank_prisoner_boss_equipment.setLayout(new BoxLayout(rank_prisoner_boss_equipment, BoxLayout.LINE_AXIS)); 
		rank_prisoner_boss_equipment.add(rank_panel);
		rank_prisoner_boss_equipment.add(boss_panel);
		rank_prisoner_boss_equipment.add(prisoners_panel);
		rank_prisoner_boss_equipment.add(equipment_panel);
		
		// Put the weapons and misc panels into one big panel
		weapons_misc_panel.add(weapons_panel);
		weapons_misc_panel.add(misc_panel);
		
		// Split the first and second panels (weapons and misc on top, rank + prisoners + boss + equipment all on the bottom)
		row1.setDividerSize(0);
	    row1.setDividerLocation(150);
	    row1.setOrientation(JSplitPane.VERTICAL_SPLIT);
	    row1.setTopComponent(weapons_misc_panel);
	    row1.setBottomComponent(rank_prisoner_boss_equipment); 
	    row1.setBorder(BorderFactory.createRaisedBevelBorder());
       
        // Set up the ammunition and rations panel
        ammo_rations_panel = create_ammo_rations(myFont, titleFont, actionListener);
        generate_panel = create_generate_button(myFont, actionListener);
        ammo_rations_generate.add(ammo_rations_panel);
        ammo_rations_generate.add(generate_panel);
        
        // Split the second and third rows
        row2.setDividerSize(0);
	    row2.setDividerLocation(200);
	    row2.setOrientation(JSplitPane.VERTICAL_SPLIT);
	    row2.setTopComponent(row1);
	    row2.setRightComponent(ammo_rations_generate);
	    row2.setBorder(BorderFactory.createRaisedBevelBorder()); 

	    // Creating the decode panel
	    decode_panel = create_decode(myFont, titleFont, actionListener);
        
        // Adding the items into the scroll pane object 
        master_panel.add(row1);
        master_panel.add(row2); 
       
        
        constant.add(master_panel, "1");
        constant.add(decode_panel, "2");
        
        
        // Call the "set up menu bar" method and add it to the main window
        menu_bar = create_bar(myFont, actionListener);
        
        // Add action listeners for the two menu items
        button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				card.show(constant, "2");
				return;
			}
		});
		
		button_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				card.show(constant, "1");
				return;
			}
		}); 
		
        this.setJMenuBar(menu_bar);
        
        scroll = new JScrollPane();
        scroll.setViewportView(constant);
        
        
        // Final touches to the GUI (setting an initial size, setting it to be visible, making it easy to close, etc.)
        this.getContentPane().add(scroll, BorderLayout.CENTER); 
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(new Dimension(1000,1000));
        this.setLocationRelativeTo(null);
        this.setVisible(true); 
	}
	
	public JPanel create_decode (Font myFont, Font titleFont, ActionListener listener)
	{
		JPanel decode = new JPanel();
		decode.setLayout(new FlowLayout());
		decode.setBorder(BorderFactory.createTitledBorder(decode.getBorder(), "Decode a password", TitledBorder.CENTER, TitledBorder.CENTER, titleFont, Color.RED));
		decode.setOpaque(true);
		decode.setBackground(Color.BLACK);
		
		// Set up the mask
		MaskFormatter format = null;
		try {
		    format = new MaskFormatter("AAAAA AAAAA AAAAA AAAAA AAAAA");
		    format.setPlaceholderCharacter(' ');
		} catch (ParseException e) {
		    e.printStackTrace();
		}       
		
		decode_TF = new JFormattedTextField(format);
		
		decode.add(decode_TF);
		decode_TF.addActionListener(listener);
		decode_TF.setFont(myFont);
		decode_TF.setForeground(Color.BLACK);
		decode_TF.setBackground(Color.WHITE);
		
		decode.add(decode_button);
		decode_button.addActionListener(listener);
		decode_button.setFont(myFont);
		decode_button.setBackground(Color.BLACK);
		decode_button.setForeground(Color.WHITE);
		
		return decode;
	}
	
	public JMenuBar create_bar (Font myFont, ActionListener actionListener)
	{
		JMenuBar bar = new JMenuBar();
		
		bar.setBackground(Color.BLACK);
        
        // Set up help tab
        help = new JMenu("Help");
        help.setForeground(Color.WHITE);
        help.setFont(myFont);
        
        // Set up about sub-tab and add the help tab to bar
        about = new JMenuItem("About");
        help.add(about);
        about.setFont(myFont);
        about.setForeground(Color.WHITE);
        about.setBackground(Color.BLACK);
        about.addActionListener(actionListener);
        bar.add(help); 
        
        // Add the change switches to change menu, then add change menu to main bar
        change = new JMenu("Switch");
        change.setFont(myFont);
        change.setForeground(Color.WHITE);
        
        button_1 = new JMenuItem("Decode a password");
        button_1.setForeground(Color.WHITE);
        button_1.setBackground(Color.BLACK);
        button_1.setFont(myFont);
        button_2 = new JMenuItem("Create a password");
        button_2.setBackground(Color.BLACK);
        button_2.setForeground(Color.WHITE);
        button_2.setFont(myFont);
        
        change.add(button_1);
        change.add(button_2);
        bar.add(change);
        
        return bar; 
	}
	
	public JPanel create_generate_button (Font normal, ActionListener listener)
	{
		JPanel generate = new JPanel();
		Font myFont = normal;
		
		generate.setLayout(new FlowLayout());
		generate.setBackground(Color.BLACK);
		generate.setOpaque(true);
		
		
		generate.add(generate_password);
		generate_password.addActionListener(listener);
		generate_password.setFont(myFont);
		generate_password.setBackground(Color.BLACK);
		generate_password.setForeground(Color.WHITE);
		
		return generate;
	}
	
	public JPanel create_ammo_rations (Font normal, Font title, ActionListener listener)
	{
		JPanel ammo_rations_panel = new JPanel();
		Font myFont = normal;
		Font titleFont = title;
		
		
		ammo_rations_panel.setLayout(new GridLayout(8,1));
		ammo_rations_panel.setBorder(BorderFactory.createTitledBorder(ammo_rations_panel.getBorder(), "Rations and Ammunition", TitledBorder.CENTER, TitledBorder.CENTER, titleFont, Color.GREEN));
	    ammo_rations_panel.setOpaque(true);
	    ammo_rations_panel.setBackground(Color.BLACK);
	    
	    // Add Beretta
	    ammo_rations_panel.add(beretta_TF);
		beretta_TF.addActionListener(listener);
		beretta_TF.setFont(myFont);
		beretta_TF.setForeground(Color.BLACK);
		beretta_TF.setBackground(Color.WHITE);
		ammo_rations_panel.add(set_beretta);
		set_beretta.addActionListener(listener);
		set_beretta.setFont(myFont); 
		set_beretta.setBackground(Color.BLACK);
		set_beretta.setForeground(Color.WHITE);
	    
	    // Add mines
		ammo_rations_panel.add(mine_TF);
		mine_TF.addActionListener(listener);
		mine_TF.setFont(myFont);
		mine_TF.setForeground(Color.BLACK);
		mine_TF.setBackground(Color.WHITE);
		ammo_rations_panel.add(set_mine);
		set_mine.addActionListener(listener);
		set_mine.setFont(myFont); 
		set_mine.setBackground(Color.BLACK);
		set_mine.setForeground(Color.WHITE);
	    
	    // Add C4
		ammo_rations_panel.add(c4_TF);
		c4_TF.addActionListener(listener);
		c4_TF.setFont(myFont);
		c4_TF.setForeground(Color.BLACK);
		c4_TF.setBackground(Color.WHITE);
		ammo_rations_panel.add(set_c4);
		set_c4.addActionListener(listener);
		set_c4.setFont(myFont); 
		set_c4.setForeground(Color.WHITE);
		set_c4.setBackground(Color.BLACK);
	    
	    // Add RC missiles
		ammo_rations_panel.add(missiles_TF);
		missiles_TF.addActionListener(listener);
		missiles_TF.setFont(myFont);
		missiles_TF.setForeground(Color.BLACK);
		missiles_TF.setBackground(Color.WHITE);
		ammo_rations_panel.add(set_missiles);
		set_missiles.addActionListener(listener);
		set_missiles.setFont(myFont); 
		set_missiles.setBackground(Color.BLACK);
		set_missiles.setForeground(Color.WHITE);
	    
	    // Add MAC-11
		ammo_rations_panel.add(ingram_TF);
		ingram_TF.addActionListener(listener);
		ingram_TF.setFont(myFont);
		ingram_TF.setForeground(Color.BLACK);
		ingram_TF.setBackground(Color.WHITE);
		ammo_rations_panel.add(set_ingram);
		set_ingram.addActionListener(listener);
		set_ingram.setFont(myFont); 
		set_ingram.setForeground(Color.WHITE);
		set_ingram.setBackground(Color.BLACK);
	    
	    // Add M79
		ammo_rations_panel.add(m79_TF);
		m79_TF.addActionListener(listener);
		m79_TF.setFont(myFont);
		m79_TF.setForeground(Color.BLACK);
		m79_TF.setBackground(Color.WHITE);
		ammo_rations_panel.add(set_m79);
		set_m79.addActionListener(listener);
		set_m79.setFont(myFont); 
		set_m79.setForeground(Color.WHITE);
		set_m79.setBackground(Color.BLACK);
	    
	    // Add RPG
		ammo_rations_panel.add(rpg_TF);
		rpg_TF.addActionListener(listener);
		rpg_TF.setFont(myFont);
		rpg_TF.setForeground(Color.BLACK);
		rpg_TF.setBackground(Color.WHITE);
		ammo_rations_panel.add(set_rpg);
		set_rpg.addActionListener(listener);
		set_rpg.setFont(myFont); 
		set_rpg.setBackground(Color.BLACK);
		set_rpg.setForeground(Color.WHITE);
	    
	    // Add rations
		ammo_rations_panel.add(rations_TF);
		rations_TF.addActionListener(listener);
		rations_TF.setFont(myFont);
		rations_TF.setForeground(Color.BLACK);
		rations_TF.setBackground(Color.WHITE);
		ammo_rations_panel.add(set_rations);
		set_rations.addActionListener(listener);
		set_rations.setFont(myFont); 
		set_rations.setBackground(Color.BLACK); 
		set_rations.setForeground(Color.WHITE);
		
		
		return ammo_rations_panel; 
	}
	
	public JPanel create_misc (Font normal, Font title, ActionListener listener)
	{
		JPanel misc_panel = new JPanel();
		Font myFont = normal;
		Font titleFont = title;
		ActionListener actionListener = listener;
		
		// Setting up the layout for the boss checkboxes and other misc stuff
		misc_panel.setLayout(new GridLayout(2,1));
		misc_panel.setBorder(BorderFactory.createTitledBorder(misc_panel.getBorder(), "Miscellaneous Events", TitledBorder.CENTER, TitledBorder.CENTER, titleFont, Color.CYAN));
	    misc_panel.setOpaque(true);
	    misc_panel.setBackground(Color.BLACK);
	    
	    misc[0] = new JCheckBox("I have been captured by the enemy!");
	    misc[1] = new JCheckBox("I have escaped and recovered my equipment!");
	    
	    // Add checkboxes to misc panel
	    for (int counter = 0; counter < 2; counter++)
	    {
	    	misc[counter].setBackground(Color.BLACK);
        	misc[counter].setFont(myFont);
        	misc[counter].setForeground(Color.WHITE);
        	misc[counter].addActionListener(actionListener); 
            misc_panel.add(misc[counter]);
	    }
		
		return misc_panel;
	}
	
	public JPanel create_equipment (Font normal, Font title, ActionListener listener)
	{
		JPanel equipment_panel = new JPanel();
		Font myFont = normal;
		Font titleFont = title;
		ActionListener actionListener = listener;
		
		// Setting up the layout for the boss checkboxes and other misc stuff
		equipment_panel.setLayout(new GridLayout(12,2));
		equipment_panel.setBorder(BorderFactory.createTitledBorder(equipment_panel.getBorder(), "Equipment Acquired", TitledBorder.CENTER, TitledBorder.CENTER, titleFont, Color.RED));
	    equipment_panel.setOpaque(true);
	    equipment_panel.setBackground(Color.BLACK);
	    
	    equipment[0] = new JCheckBox("Cigarettes");
	    equipment[1] = new JCheckBox("Access Card #1");
	    equipment[2] = new JCheckBox("Access Card #2");
	    equipment[3] = new JCheckBox("Access Card #3");
	    equipment[4] = new JCheckBox("Access Card #4");
	    equipment[5] = new JCheckBox("Access Card #5");
	    equipment[6] = new JCheckBox("Access Card #6");
	    equipment[7] = new JCheckBox("Access Card #7");
	    equipment[8] = new JCheckBox("Access Card #8");
	    equipment[9] = new JCheckBox("Binoculars");
	    equipment[10] = new JCheckBox("Gas Mask");
	    equipment[11] = new JCheckBox("Cardboard Box");
	    equipment[12] = new JCheckBox("Radio Transmitter");
	    equipment[13] = new JCheckBox("Enemy Uniform");
	    equipment[14] = new JCheckBox("Infrared Goggles");
	    equipment[15] = new JCheckBox("Bomb Blast Suit");
	    equipment[16] = new JCheckBox("Iron Glove");
	    equipment[17] = new JCheckBox("Land Mine Detector");
	    equipment[18] = new JCheckBox("Communications Antenna");
	    equipment[19] = new JCheckBox("Body Armor");
	    equipment[20] = new JCheckBox("Antidote");
	    equipment[21] = new JCheckBox("Compass");
	    equipment[22] = new JCheckBox("Flashlight");
	    equipment[23] = new JCheckBox("Oxygen Tank"); 
	    
	    
	    // Add checkboxes to equipment panel
	    for (int counter = 0; counter < 24; counter++)
	    {
	    	equipment[counter].setBackground(Color.BLACK);
        	equipment[counter].setFont(myFont);
        	equipment[counter].setForeground(Color.WHITE);
        	equipment[counter].addActionListener(actionListener); 
            equipment_panel.add(equipment[counter]);
	    }
	    
	    return equipment_panel;
	}
	
	public JPanel create_weapons (Font normal, Font title, ActionListener listener)
	{
		JPanel weapons_panel = new JPanel();
		Font myFont = normal;
		Font titleFont = title;
		ActionListener actionListener = listener;
		
		// Setting up the layout for the boss checkboxes and other misc stuff
		weapons_panel.setLayout(new GridLayout(4,2));
		weapons_panel.setBorder(BorderFactory.createTitledBorder(weapons_panel.getBorder(), "Weapons Acquired", TitledBorder.CENTER, TitledBorder.CENTER, titleFont, Color.CYAN));
	    weapons_panel.setOpaque(true);
	    weapons_panel.setBackground(Color.BLACK);
	    
	    weapons[0] = new JCheckBox("Beretta M92F");
	    weapons[1] = new JCheckBox("Silencer");
	    weapons[2] = new JCheckBox("Ingram MAC-11");
	    weapons[3] = new JCheckBox("M79 Grenade Launcher");
	    weapons[4] = new JCheckBox("RPG-7V Rocket Launcher");
	    weapons[5] = new JCheckBox("C4 Plastic Explosives"); 
	    weapons[6] = new JCheckBox("Land Mine");
	    weapons[7] = new JCheckBox("Remote-Controlled Missile Launcher");
	    
	    
	    // Add checkboxes to weapons panel
	    for (int counter = 0; counter < 8; counter++)
	    {
	    	weapons[counter].setBackground(Color.BLACK);
        	weapons[counter].setFont(myFont);
        	weapons[counter].setForeground(Color.WHITE);
        	weapons[counter].addActionListener(actionListener); 
            weapons_panel.add(weapons[counter]);
	    }
	    
	    
	    return weapons_panel;
	}
	
	public JPanel create_rank (Font normal, Font title)
	{
		JPanel rank_panel = new JPanel(); 
		Font myFont = normal;
		Font titleFont = title;
		
		// Setting up the radio buttons for the Snake's rank along with ButtonGroup variable 
		rank_panel.setLayout(new GridLayout(4,1)); 
		ButtonGroup group = new ButtonGroup();
		
		// Add the borders to the radio buttons panel 
        rank_panel.setBorder(BorderFactory.createTitledBorder(rank_panel.getBorder(), "Rank", TitledBorder.CENTER, TitledBorder.CENTER, titleFont, Color.RED));
        rank_panel.setOpaque(true);
        rank_panel.setBackground(Color.BLACK);
	    
	    
		// Set the radio button texts
        rank[0] = new JRadioButton("Rank *");
        rank[1] = new JRadioButton("Rank **");
        rank[2] = new JRadioButton("Rank ***");
        rank[3] = new JRadioButton("Rank ****");
        
        // Add the radio buttons to the panel with font, etc. and group them
        for (int counter = 0; counter < 4; counter++) {
        	rank[counter].setBackground(Color.BLACK);
        	rank[counter].setFont(myFont);
        	rank[counter].setForeground(Color.WHITE);
            rank_panel.add(rank[counter]);
            group.add(rank[counter]);
        }
        
        
        // Add item listeners for each radio button (ranks 1, 2, 3, and 4 respectively)
        rank[0].addItemListener(new ItemListener() 
        {
            public void itemStateChanged(ItemEvent event) 
            {         
               String one = Integer.toBinaryString(1);
               one = StringUtils.leftPad(one, 3, "0");
               byte_0 = byte_0.substring(0,2) + one;
            }           
         });
        
        rank[1].addItemListener(new ItemListener() 
        {
            public void itemStateChanged(ItemEvent event) 
            {             
            	String two = Integer.toBinaryString(2);
                two = StringUtils.leftPad(two, 3, "0");
                byte_0 = byte_0.substring(0,2) + two; 
            }           
        });
        
        rank[2].addItemListener(new ItemListener() 
        {
            public void itemStateChanged(ItemEvent event) 
            {             
            	String three = Integer.toBinaryString(3);
                three = StringUtils.leftPad(three, 3, "0");
                byte_0 = byte_0.substring(0,2) + three; 
            }           
        });
        
        rank[3].addItemListener(new ItemListener() 
        {
            public void itemStateChanged(ItemEvent event) 
            {             
            	String four = Integer.toBinaryString(4);
                four = StringUtils.leftPad(four, 3, "0");
                byte_0 = byte_0.substring(0,2) + four; 
            }           
        }); 
        
       
        
        
        return rank_panel;
	}
	
	public JPanel create_boss (Font normal, Font title, ActionListener listener)
	{
		JPanel boss_panel = new JPanel();
		Font myFont = normal;
		Font titleFont = title;
		ActionListener actionListener = listener;
		
		// Setting up the layout for the boss checkboxes and other misc stuff
		boss_panel.setLayout(new GridLayout(5,2));
		boss_panel.setBorder(BorderFactory.createTitledBorder(boss_panel.getBorder(), "Bosses Defeated", TitledBorder.CENTER, TitledBorder.CENTER, titleFont, Color.RED));
	    boss_panel.setOpaque(true);
	    boss_panel.setBackground(Color.BLACK);
	    
	    boss[0] = new JCheckBox("Shotgunner");
	    boss[1] = new JCheckBox("Machine Gun Kid");
	    boss[2] = new JCheckBox("Tank");
	    boss[3] = new JCheckBox("Twin Shot");
	    boss[4] = new JCheckBox("Bull Tank");
	    boss[5] = new JCheckBox("Coward Duck");
	    boss[6] = new JCheckBox("Arnold");
	    boss[7] = new JCheckBox("Super Computer");
	    boss[8] = new JCheckBox("Fire Trooper");
	    boss[9] = new JCheckBox("Vermon CaTaffy");
	    
	    // Add checkboxes to boss panel
	    for (int counter = 0; counter < 10; counter++)
	    {
	    	boss[counter].setBackground(Color.BLACK);
        	boss[counter].setFont(myFont);
        	boss[counter].setForeground(Color.WHITE);
        	boss[counter].addActionListener(actionListener); 
            boss_panel.add(boss[counter]);
	    }
	    
		return boss_panel;
		
	}
	
	public JPanel create_prisoners (Font normal, Font title, ActionListener listener)
	{
		JPanel prisoners_panel = new JPanel();
		Font myFont = normal;
		Font titleFont = title;
		ActionListener actionListener = listener;
		
		// Set up the layout
		prisoners_panel.setLayout(new GridLayout(5,2));
		prisoners_panel.setBorder(BorderFactory.createTitledBorder(prisoners_panel.getBorder(), "Hostages Rescued", TitledBorder.CENTER, TitledBorder.CENTER, titleFont, Color.RED));
	    prisoners_panel.setOpaque(true);
	    prisoners_panel.setBackground(Color.BLACK);
	    
	    prisoners[0] = new JCheckBox("Prisoner #1");
	    prisoners[1] = new JCheckBox("Prisoner #2");
	    prisoners[2] = new JCheckBox("Prisoner #3");
	    prisoners[3] = new JCheckBox("Prisoner #4");
	    prisoners[4] = new JCheckBox("Prisoner #5");
	    prisoners[5] = new JCheckBox("Prisoner #6");
	    prisoners[6] = new JCheckBox("Prisoner #7");
	    prisoners[7] = new JCheckBox("Prisoner #8");
	    prisoners[8] = new JCheckBox("Prisoner #9");
	    prisoners[9] = new JCheckBox("Prisoner #10");
	    prisoners[10] = new JCheckBox("Prisoner #11");
	    prisoners[11] = new JCheckBox("Prisoner #12");
	    prisoners[12] = new JCheckBox("Prisoner #13");
	    prisoners[13] = new JCheckBox("Prisoner #14");
	    prisoners[14] = new JCheckBox("Prisoner #15");
	    prisoners[15] = new JCheckBox("Prisoner #16");
	    prisoners[16] = new JCheckBox("Prisoner #17");
	    prisoners[17] = new JCheckBox("Prisoner #18");
	    prisoners[18] = new JCheckBox("Prisoner #19");
	    prisoners[19] = new JCheckBox("Prisoner #20");
	    prisoners[20] = new JCheckBox("Prisoner #21");
	    prisoners[21] = new JCheckBox("Prisoner #22");
	    
	    // Add checkboxes
	    for (int counter = 0; counter < 22; counter++)
	    {
	    	prisoners[counter].setBackground(Color.BLACK);
        	prisoners[counter].setFont(myFont);
        	prisoners[counter].setForeground(Color.WHITE);
        	prisoners[counter].addActionListener(actionListener); 
            prisoners_panel.add(prisoners[counter]);
	    }
	    
	    return prisoners_panel;
	    
	}
	
	class ActionHandler implements ActionListener { 
		
		public void actionPerformed(ActionEvent event) 
		{
			// Event listeners for buttons, return statements are necessary or else casting errors will occur
			if (event.getSource() == about)
			{
				JOptionPane.showMessageDialog(null, "Copyright Thomas Wong AKA Frost byte, July 2017"); 
				return;
			}
			
			else if (event.getSource() == decode_button)
			{
				String input = decode_TF.getText();
				original_password = input; // Save copy
				char [ ] array = input.toCharArray();
				
				// Check formatting
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isLetter(array[counter]) == false && Character.isDigit(array[counter]) == false && input.charAt(counter) != ' ')
					{
						JOptionPane.showMessageDialog(null, "Incorrect format");
						return;
					}
					
					else if ((input.charAt(counter) > '6' && input.charAt(counter) < '1') && Character.isLetter(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(null, "Digits in the password cannot be larger than 6 or smaller than 1");
						return;
					}
					
					else if (input.charAt(counter) == ' ' && (counter != 5 && counter != 11 && counter != 17 && counter != 23))
					{
						JOptionPane.showMessageDialog(null, "Spaces can only appear every 5 characters");
						return;
					}
					
				}
				
				/** Decoding process
				 * 
				 * 1. Convert each letter of the password to its 5-bit binary representation (excluding the last character)
				 * 2. Update "bits" string if correction was made in determine_rank method
				 * 3. Determine Snake's rank and set it equal to rank; add an error to errors linked list if need be
				 * 4. Determine which bosses have been defeated and return the "head" of the created bosses linked list
				 * 5. Determine which prisoners Snake has rescued and return the "head" of the created prisoners linked list
				 * 6. Like step 5, determine what equipment Snake has acquired and add it to a linked list, then return head of list
				 * 7. Like step 6, determine misc events and add to linked list, then return head of list
				 * 8. Repeat step 7, except this time determine weapons acquired and return head of weapons list
				 * 9. Determine ammunition and rations, correct password if need be and return head of list 
				 *   (add errors to errors linked list), then re-update bit string if changes were made
				 * 10. Verify checksum, fix if need be
				 **/
				
				char checksum = input.charAt(input.length() - 1); // Do not include checksum
				String bits = bossesLL.convert_to_bits(input.substring(0, input.length()-1)); // Step 1 
				bits = bossesLL.determine_rank(bits); // Step 2
				snake_rank = bossesLL.copy; // Step 3
				bosses_head = bossesLL.determine_boss(bits); // Step 4
				prisoners_head = prisonersLL.determine_prisoners(bits); // Step 5
				equipment_head = equipmentLL.determine_equipment(bits); // Step 6
				misc_head = miscLL.determine_misc(bits); // Step 7
				weapons_head = weaponsLL.determine_weapons(bits); // Step 8
				ammo_head = ammoLL.determine_ammo(bits); // Step 9
				bits = ammoLL.bit_pattern; // Step 9 again
				Verify.calculate_sum(bits,checksum); // Step 10
				corrected = Verify.correct;
				
				// Create new window to print out password information
				new print();
				
				// Set all heads equal to null
				bosses_head = StringNode.delete(bosses_head);
				prisoners_head = StringNode.delete(prisoners_head);
				equipment_head = StringNode.delete(equipment_head);
				misc_head = StringNode.delete(misc_head);
				weapons_head = StringNode.delete(weapons_head);
				ammo_head = StringIntegerNode.delete(ammo_head);
				errors = StringIntegerNode.delete(errors);
				
			return;
			}
			
		    else if (event.getSource() == set_beretta)
			{
				String input = beretta_TF.getText();
				char [ ] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(null, "Input can only have numbers");  
						return; 
					}
				}
				
				int input_int = Integer.parseInt(input);
				
				if (input_int < 0 || input_int > 255)
				{
					JOptionPane.showMessageDialog(null, "Snake can only carry between 0 to 255 inclusive Beretta rounds"); 
					return;
				}
				
				String result = Integer.toBinaryString(input_int);
				result = StringUtils.leftPad(result, 8, "0");
				
				byte_10 = result.substring(result.length() - 5); // Take the last 5 "bits" of the result string
				// Retrieve second and third "bits" from result string
				// Retrieve first "bit" from result string
				
				byte_21 = result.substring(1,3) + byte_21.substring(2);
				byte_23 = byte_23.charAt(0) + result.substring(0,1) + byte_23.substring(2);
				
				return;
				
			}
			
			else if (event.getSource() == set_mine)
			{
				String input = mine_TF.getText();
				char [ ] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(null, "Input can only have numbers");  
						return; 
					}
				}
				
				int input_int = Integer.parseInt(input);
				
				if (input_int < 0 || input_int > 20)
				{
					JOptionPane.showMessageDialog(null, "Snake can only carry between 0 to 20 inclusive land mines");
					return;
				}
				
				String result = Integer.toBinaryString(input_int);
				byte_13 = StringUtils.leftPad(result, 5, "0");
				return;
			}
			
			else if (event.getSource() == set_c4)
			{
				String input = c4_TF.getText();
				char [ ] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(null, "Input can only have numbers");  
						return; 
					}
				}
				
				int input_int = Integer.parseInt(input);
				
				if (input_int < 0 || input_int > 20)
				{
					JOptionPane.showMessageDialog(null, "Snake can only carry between 0 to 20 inclusive C4 Plastic Explosives");
					return;
				}
				
				String result = Integer.toBinaryString(input_int);
				byte_12 = StringUtils.leftPad(result, 5, "0");
				return;
			}
			
			else if (event.getSource() == set_missiles)
			{
				String input = missiles_TF.getText();
				char [ ] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(null, "Input can only have numbers");  
						return; 
					}
				}
				
				int input_int = Integer.parseInt(input);
				
				if (input_int < 0 || input_int > 20)
				{
					JOptionPane.showMessageDialog(null, "Snake can only carry between 0 to 20 inclusive missiles");
					return;
				}
				
				String result = Integer.toBinaryString(input_int);
				byte_11 = StringUtils.leftPad(result, 5, "0");
				return;
			}
			
			else if (event.getSource() == set_ingram)
			{
				String input = ingram_TF.getText();
				char [ ] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(null, "Input can only have numbers");  
						return; 
					}
				}
				
				int input_int = Integer.parseInt(input);
				
				if (input_int < 0 || input_int > 255)
				{
					JOptionPane.showMessageDialog(null, "Snake can only carry between 0 to 255 inclusive MAC-11 rounds");
					return;
				}
				
				String result = Integer.toBinaryString(input_int);
				result = StringUtils.leftPad(result, 8, "0"); 
				
				byte_14 = result.substring(result.length() - 5);
				byte_17 = byte_17.substring(0,2) + result.substring(0,3); 
				
				return;
			}
			
			else if (event.getSource() == set_m79)
			{
				String input = m79_TF.getText();
				char [ ] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(null, "Input can only have numbers");  
						return; 
					}
				}
				
				int input_int = Integer.parseInt(input);
				
				if (input_int < 0 || input_int > 90)
				{
					JOptionPane.showMessageDialog(null, "Snake can only carry between 0 to 90 inclusive grenades");
					return;
				}
				
				String result = Integer.toBinaryString(input_int);
				result = StringUtils.leftPad(result, 7, "0"); 
				
				byte_15 = result.substring(result.length() - 5);
				byte_17 = result.substring(0,2) + byte_17.substring(2);
				
				return;
			}
			
			else if (event.getSource() == set_rpg)
			{
				String input = rpg_TF.getText();
				char [ ] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(null, "Input can only have numbers");  
						return; 
					}
				}
				
				int input_int = Integer.parseInt(input);
				
				if (input_int < 0 || input_int > 30) 
				{
					JOptionPane.showMessageDialog(null, "Snake can only carry between 0 to 30 inclusive rockets");
					return;
				}
				
				String result = Integer.toBinaryString(input_int); 
				byte_16 = StringUtils.leftPad(result, 5, "0"); 
				return;
			}
			
			else if (event.getSource() == set_rations)
			{
				String input = rations_TF.getText();
				char [ ] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(null, "Input can only have numbers");  
						return; 
					}
				}
				
				int input_int = Integer.parseInt(input);
				
				if (input_int < 0 || input_int > 15)
				{
					JOptionPane.showMessageDialog(null, "Snake can only carry between 0 to 15 inclusive rations");
					return;
				}
				
				String result = Integer.toBinaryString(input_int); 
				byte_9 = StringUtils.leftPad(result, 5, "0"); 
				return;
			}
			
			else if (event.getSource() == generate_password)
			{
				String bit_pattern = byte_0 + byte_1 + byte_2 + byte_3 + byte_4 + byte_5 + byte_6 + byte_7
				+ byte_8 + byte_9 + byte_10 + byte_11 + byte_12 + byte_13 + byte_14 + byte_15 + byte_16 +
				byte_17 + byte_18 + byte_19 + byte_20 + byte_21 + byte_22 + byte_23; 
				 
				Encode.create_first_24(bit_pattern);
				return;
				
			}
			
/*********************************************************************************/		
			
			// Event handler for checkboxes 
			JCheckBox source = (JCheckBox)event.getSource();
			
			if (source.isSelected())
			{
				
			// Bosses	
			if (source.getText().equals("Shotgunner")) 
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[2] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Machine Gun Kid"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[4] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Tank"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[0] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Twin Shot"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[3] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Bull Tank"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[1] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Coward Duck"))
			{
				char [ ] temporary = byte_18.toCharArray();
				temporary[2] = '1';
				byte_18 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Arnold"))
			{
				char [ ] temporary = byte_18.toCharArray();
				temporary[4] = '1';
				byte_18 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Super Computer"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[1] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Fire Trooper"))
			{
				char [ ] temporary = byte_18.toCharArray();
				temporary[3] = '1';
				byte_18 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Vermon CaTaffy"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[0] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			// Weapons
			else if (source.getText().equals("Beretta M92F"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[4] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Silencer"))
			{
				char [ ] temporary = byte_19.toCharArray();
				temporary[2] = '1';
				byte_19 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Ingram MAC-11"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[0] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("M79 Grenade Launcher"))
			{
				char [ ] temporary = byte_19.toCharArray();
				temporary[4] = '1';
				byte_19 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("RPG-7V Rocket Launcher"))
			{
				char [ ] temporary = byte_19.toCharArray();
				temporary[3] = '1';
				byte_19 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("C4 Plastic Explosives"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[2] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Land Mine"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[3] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Remote-Controlled Missile Launcher"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[1] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			// Equipment
			else if (source.getText().equals("Cigarettes"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[4] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Access Card #1"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[4] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Access Card #2"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[3] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Access Card #3"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[2] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Access Card #4"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[1] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Access Card #5"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[0] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Access Card #6"))
			{
				char [ ] temporary = byte_23.toCharArray();
				temporary[4] = '1';
				byte_23 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Access Card #7"))
			{
				char [ ] temporary = byte_23.toCharArray();
				temporary[3] = '1';
				byte_23 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Access Card #8"))
			{
				char [ ] temporary = byte_23.toCharArray();
				temporary[2] = '1';
				byte_23 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Binoculars"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[2] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Gas Mask"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[3] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Cardboard Box"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[1] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Radio Transmitter"))
			{
				char [ ] temporary = byte_22.toCharArray();
				temporary[1] = '1';
				byte_22 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Enemy Uniform"))
			{
				char [ ] temporary = byte_18.toCharArray();
				temporary[1] = '1';
				byte_18 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Infrared Goggles"))
			{
				char [ ] temporary = byte_18.toCharArray();
				temporary[0] = '1';
				byte_18 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Bomb Blast Suit"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[0] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Iron Glove"))
			{
				char [ ] temporary = byte_22.toCharArray();
				temporary[0] = '1';
				byte_22 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Land Mine Detector"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[4] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Communications Antenna"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[2] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Body Armor"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[3] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Antidote"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[1] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Compass"))
			{
				char [ ] temporary = byte_19.toCharArray();
				temporary[1] = '1';
				byte_19 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Flashlight"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[0] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Oxygen Tank"))
			{
				char [ ] temporary = byte_19.toCharArray();
				temporary[0] = '1';
				byte_19 = String.valueOf(temporary);
			}
			
			// Prisoners
			else if (source.getText().equals("Prisoner #1"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[0] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #2"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[1] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #3"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[2] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #4"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[3] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #5"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[4] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #6"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[0] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #7"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[1] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #8"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[2] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #9"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[3] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #10"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[4] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #11"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[0] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #12"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[1] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #13"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[2] = '1';
				byte_5 = String.valueOf(temporary);
			}
			else if (source.getText().equals("Prisoner #14"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[3] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #15"))
			{
				char [ ] temporary = byte_20.toCharArray();
				temporary[3] = '1';
				byte_20 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #16"))
			{
				char [ ] temporary = byte_20.toCharArray();
				temporary[4] = '1';
				byte_20 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #17"))
			{
				char [ ] temporary = byte_21.toCharArray();
				temporary[2] = '1';
				byte_21 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #18"))
			{
				char [ ] temporary = byte_21.toCharArray();
				temporary[3] = '1';
				byte_21 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #19"))
			{
				char [ ] temporary = byte_21.toCharArray();
				temporary[4] = '1';
				byte_21 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #20"))
			{
				char [ ] temporary = byte_22.toCharArray();
				temporary[2] = '1';
				byte_22 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #21"))
			{
				char [ ] temporary = byte_22.toCharArray();
				temporary[3] = '1';
				byte_22 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("Prisoner #22"))
			{
				char [ ] temporary = byte_22.toCharArray();
				temporary[4] = '1';
				byte_22 = String.valueOf(temporary);
			}
			
			// Special Events 
			else if (source.getText().equals("I have been captured by the enemy!"))
			{
				char [ ] temporary = byte_20.toCharArray();
				temporary[0] = '1';
				byte_20 = String.valueOf(temporary);
			}
			
			else if (source.getText().equals("I have escaped and recovered my equipment!"))
			{
				char [ ] temporary = byte_23.toCharArray();
				temporary[0] = '1';
				byte_23 = String.valueOf(temporary);
			}
			
			
			
			
			
			}
			
			else // Checkbox has been "un-checked"
			{
				
				// Bosses
				if (source.getText().equals("Shotgunner")) 
				{
					char [ ] temporary = byte_1.toCharArray();
					temporary[2] = '0';
					byte_1 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Machine Gun Kid"))
				{
					char [ ] temporary = byte_1.toCharArray();
					temporary[4] = '0';
					byte_1 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Tank"))
				{
					char [ ] temporary = byte_1.toCharArray();
					temporary[0] = '0';
					byte_1 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Twin Shot"))
				{
					char [ ] temporary = byte_1.toCharArray();
					temporary[3] = '0';
					byte_1 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Bull Tank"))
				{
					char [ ] temporary = byte_1.toCharArray();
					temporary[1] = '0';
					byte_1 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Coward Duck"))
				{
					char [ ] temporary = byte_18.toCharArray();
					temporary[2] = '0';
					byte_18 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Arnold"))
				{
					char [ ] temporary = byte_18.toCharArray();
					temporary[4] = '0';
					byte_18 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Super Computer"))
				{
					char [ ] temporary = byte_0.toCharArray();
					temporary[1] = '0';
					byte_0 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Fire Trooper"))
				{
					char [ ] temporary = byte_18.toCharArray();
					temporary[3] = '0';
					byte_18 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Vermon CaTaffy"))
				{
					char [ ] temporary = byte_0.toCharArray();
					temporary[0] = '0';
					byte_0 = String.valueOf(temporary);
				} 
				
				// Weapons
				else if (source.getText().equals("Beretta M92F"))
				{
					char [ ] temporary = byte_2.toCharArray();
					temporary[4] = '0';
					byte_2 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Silencer"))
				{
					char [ ] temporary = byte_19.toCharArray();
					temporary[2] = '0';
					byte_19 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Ingram MAC-11"))
				{
					char [ ] temporary = byte_2.toCharArray();
					temporary[0] = '0';
					byte_2 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("M79 Grenade Launcher"))
				{
					char [ ] temporary = byte_19.toCharArray();
					temporary[4] = '0';
					byte_19 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("RPG-7V Rocket Launcher"))
				{
					char [ ] temporary = byte_19.toCharArray();
					temporary[3] = '0';
					byte_19 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("C4 Plastic Explosives"))
				{
					char [ ] temporary = byte_2.toCharArray();
					temporary[2] = '0';
					byte_2 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Land Mine"))
				{
					char [ ] temporary = byte_2.toCharArray();
					temporary[3] = '0';
					byte_2 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Remote-Controlled Missile Launcher"))
				{
					char [ ] temporary = byte_2.toCharArray();
					temporary[1] = '0';
					byte_2 = String.valueOf(temporary); 
				}
				
				// Equipment
				else if (source.getText().equals("Cigarettes"))
				{
					char [ ] temporary = byte_7.toCharArray();
					temporary[4] = '0';
					byte_7 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Access Card #1"))
				{
					char [ ] temporary = byte_6.toCharArray();
					temporary[4] = '0';
					byte_6 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Access Card #2"))
				{
					char [ ] temporary = byte_6.toCharArray();
					temporary[3] = '0';
					byte_6 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Access Card #3"))
				{
					char [ ] temporary = byte_6.toCharArray();
					temporary[2] = '0';
					byte_6 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Access Card #4"))
				{
					char [ ] temporary = byte_6.toCharArray();
					temporary[1] = '0';
					byte_6 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Access Card #5"))
				{
					char [ ] temporary = byte_6.toCharArray();
					temporary[0] = '0';
					byte_6 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Access Card #6"))
				{
					char [ ] temporary = byte_23.toCharArray();
					temporary[4] = '0';
					byte_23 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Access Card #7"))
				{
					char [ ] temporary = byte_23.toCharArray();
					temporary[3] = '0';
					byte_23 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Access Card #8"))
				{
					char [ ] temporary = byte_23.toCharArray();
					temporary[2] = '0';
					byte_23 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Binoculars"))
				{
					char [ ] temporary = byte_7.toCharArray();
					temporary[2] = '0';
					byte_7 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Gas Mask"))
				{
					char [ ] temporary = byte_7.toCharArray();
					temporary[3] = '0';
					byte_7 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Cardboard Box"))
				{
					char [ ] temporary = byte_7.toCharArray();
					temporary[1] = '0';
					byte_7 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Radio Transmitter"))
				{
					char [ ] temporary = byte_22.toCharArray();
					temporary[1] = '0';
					byte_22 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Enemy Uniform"))
				{
					char [ ] temporary = byte_18.toCharArray();
					temporary[1] = '0';
					byte_18 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Infrared Goggles"))
				{
					char [ ] temporary = byte_18.toCharArray();
					temporary[0] = '0';
					byte_18 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Bomb Blast Suit"))
				{
					char [ ] temporary = byte_7.toCharArray();
					temporary[0] = '0';
					byte_7 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Iron Glove"))
				{
					char [ ] temporary = byte_22.toCharArray();
					temporary[0] = '0';
					byte_22 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Land Mine Detector"))
				{
					char [ ] temporary = byte_8.toCharArray();
					temporary[4] = '0';
					byte_8 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Communications Antenna"))
				{
					char [ ] temporary = byte_8.toCharArray();
					temporary[2] = '0';
					byte_8 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Body Armor"))
				{
					char [ ] temporary = byte_8.toCharArray();
					temporary[3] = '0';
					byte_8 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Antidote"))
				{
					char [ ] temporary = byte_8.toCharArray();
					temporary[1] = '0';
					byte_8 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Compass"))
				{
					char [ ] temporary = byte_19.toCharArray();
					temporary[1] = '0';
					byte_19 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Flashlight"))
				{
					char [ ] temporary = byte_8.toCharArray();
					temporary[0] = '0';
					byte_8 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Oxygen Tank"))
				{
					char [ ] temporary = byte_19.toCharArray();
					temporary[0] = '0';
					byte_19 = String.valueOf(temporary);
				}
				
				// Prisoners
				else if (source.getText().equals("Prisoner #1"))
				{
					char [ ] temporary = byte_3.toCharArray();
					temporary[0] = '0';
					byte_3 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #2"))
				{
					char [ ] temporary = byte_3.toCharArray();
					temporary[1] = '0';
					byte_3 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #3"))
				{
					char [ ] temporary = byte_3.toCharArray();
					temporary[2] = '0';
					byte_3 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #4"))
				{
					char [ ] temporary = byte_3.toCharArray();
					temporary[3] = '0';
					byte_3 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #5"))
				{
					char [ ] temporary = byte_3.toCharArray();
					temporary[4] = '0';
					byte_3 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #6"))
				{
					char [ ] temporary = byte_4.toCharArray();
					temporary[0] = '0';
					byte_4 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #7"))
				{
					char [ ] temporary = byte_4.toCharArray();
					temporary[1] = '0';
					byte_4 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #8"))
				{
					char [ ] temporary = byte_4.toCharArray();
					temporary[2] = '0';
					byte_4 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #9"))
				{
					char [ ] temporary = byte_4.toCharArray();
					temporary[3] = '0';
					byte_4 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #10"))
				{
					char [ ] temporary = byte_4.toCharArray();
					temporary[4] = '0';
					byte_4 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #11"))
				{
					char [ ] temporary = byte_5.toCharArray();
					temporary[0] = '0';
					byte_5 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #12"))
				{
					char [ ] temporary = byte_5.toCharArray();
					temporary[1] = '0';
					byte_5 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #13"))
				{
					char [ ] temporary = byte_5.toCharArray();
					temporary[2] = '0';
					byte_5 = String.valueOf(temporary);
				}
				else if (source.getText().equals("Prisoner #14"))
				{
					char [ ] temporary = byte_5.toCharArray();
					temporary[3] = '0';
					byte_5 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #15"))
				{
					char [ ] temporary = byte_20.toCharArray();
					temporary[3] = '0';
					byte_20 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #16"))
				{
					char [ ] temporary = byte_20.toCharArray();
					temporary[4] = '0';
					byte_20 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #17"))
				{
					char [ ] temporary = byte_21.toCharArray();
					temporary[2] = '0';
					byte_21 = String.valueOf(temporary); 
				}
				
				else if (source.getText().equals("Prisoner #18"))
				{
					char [ ] temporary = byte_21.toCharArray();
					temporary[3] = '0';
					byte_21 = String.valueOf(temporary);
					
				}
				
				else if (source.getText().equals("Prisoner #19"))
				{
					char [ ] temporary = byte_21.toCharArray();
					temporary[4] = '0';
					byte_21 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #20"))
				{
					char [ ] temporary = byte_22.toCharArray();
					temporary[2] = '0';
					byte_22 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #21"))
				{
					char [ ] temporary = byte_22.toCharArray();
					temporary[3] = '0';
					byte_22 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("Prisoner #22"))
				{
					char [ ] temporary = byte_22.toCharArray();
					temporary[4] = '0';
					byte_22 = String.valueOf(temporary);
				}
				
				// Special Events 
				else if (source.getText().equals("I've been captured by the enemy!"))
				{
					char [ ] temporary = byte_20.toCharArray();
					temporary[0] = '0'; 
					byte_20 = String.valueOf(temporary);
				}
				
				else if (source.getText().equals("I've escaped and recovered my equipment!"))
				{
					char [ ] temporary = byte_23.toCharArray();
					temporary[0] = '0';
					byte_23 = String.valueOf(temporary);
				}
				
			}
			
			
		}
		
	}
	
	public static void main (String[] args)
	{
		UIManager.put("OptionPane.background", Color.BLACK);
		UIManager.put("Panel.background", Color.BLACK); 
		UIManager.put("OptionPane.messageFont", new Font("Metal Gear", Font.PLAIN, 14)); 
		UIManager.put("OptionPane.buttonFont", new Font("Metal Gear", Font.PLAIN, 12)); 
		UIManager.put("OptionPane.messageForeground", Color.WHITE);
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Driver();
			} 
		}); 
		
		
	}

}
