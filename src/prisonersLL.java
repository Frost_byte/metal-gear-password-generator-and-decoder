public class prisonersLL {
	
	// Singly linked list to keep track of prisoners Snake has freed
	static StringNode head;
	
	public prisonersLL() // Constructor for the linked list, initializes head to null
	{
		head = null;
	}
	
	// Determines which prisoners Snake has freed
	static StringNode determine_prisoners (String password_bits)
	{
		new prisonersLL();
		char [ ] array = password_bits.toCharArray();
		
		
		
		if (array[15] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #1");
		}
		
		if (array[16] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #2");
		}
		
		if (array[17] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #3");
		}
		
		if (array[18] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #4");
		}
		
		if (array[19] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #5");
		}
		
		if (array[20] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #6");
		}
		
		if (array[21] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #7");
		}
		
		if (array[22] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #8");
		}
		
		if (array[23] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #9");
		}
		
		if (array[24] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #10");
		}
		
		if (array[25] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #11");
		}
		
		if (array[26] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #12");
		}
		
		if (array[27] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #13");
		}
		
		if (array[28] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #14");
		}
		
		if (array[103] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #15");
		}
		
		if (array[104] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #16");
		}
		
		if (array[107] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #17");
		}
		
		if (array[108] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #18");
		}
		
		if (array[109] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #19");
		}
		
		if (array[112] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #20");
		}
		
		if (array[113] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #21");
		}
		
		if (array[114] == '1')
		{
			head = StringNode.addToFront(head, "Prisoner #22"); 
		}
		
		return head;
		
	}

}
