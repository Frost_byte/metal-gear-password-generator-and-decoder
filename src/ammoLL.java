public class ammoLL {
	
	static StringIntegerNode head;
	static String bit_pattern;
	
	public ammoLL()
	{
		head = null;
	}
	
	public static StringIntegerNode determine_ammo (String bits)
	{
		new ammoLL();
		
		// Bits for Beretta M92F (max is 255)
		
		head = StringIntegerNode.addToFront(head, "Beretta M92F", Integer.parseInt( bits.charAt(116) + bits.substring(105,107) + bits.substring(50,55) ,2));
		
		// Bits for Ingram MAC-11 (max is 255)
		
		head = StringIntegerNode.addToFront(head, "Ingram MAC-11", Integer.parseInt( bits.substring(87,90) + bits.substring(70,75) ,2));
		
		// Bits for RPG-7V Rocket Launcher (cannot be more than 30 rounds!), correct if need be
		
		if (Integer.parseInt(bits.substring(80,85),2) > 30)
		{
			
			Driver.errors = StringIntegerNode.addToFront(Driver.errors, "RPG-7V Rockets", Integer.parseInt(bits.substring(80,85),2));
			String correction = "11110"; // Give Snake 30 rockets
			bits = bits.substring(0,80) + correction + bits.substring(85);
		}
		
		head = StringIntegerNode.addToFront(head, "RPG-7V Rocket Launcher", Integer.parseInt( bits.substring(80,85) ,2));
		
		// Land Mines (cannot be more than 20!)
		
		if (Integer.parseInt(bits.substring(65,70),2) > 20)
		{
			Driver.errors = StringIntegerNode.addToFront(Driver.errors, "Land Mines", Integer.parseInt(bits.substring(65,70),2));
			String correction = "10100"; // Give Snake 20 mines
			bits = bits.substring(0,65) + correction + bits.substring(70);
		}
		
		head = StringIntegerNode.addToFront(head, "Land Mines", Integer.parseInt( bits.substring(65,70) ,2));
		
		// Remote controlled missiles (cannot be more than 20!)
		
		if (Integer.parseInt(bits.substring(55,60),2) > 20)
		{
			Driver.errors = StringIntegerNode.addToFront(Driver.errors, "Remote-controlled missile launcher", Integer.parseInt(bits.substring(65,70),2));
			String correction = "10100"; // Give Snake 20 missiles
			bits = bits.substring(0,55) + correction + bits.substring(60); 
		}
		
		head = StringIntegerNode.addToFront(head, "Remote-controlled missile launcher", Integer.parseInt( bits.substring(55,60) ,2));
		
		// C4 Plastic Explosives (cannot be more than 20!)
		
		if (Integer.parseInt(bits.substring(60,65),2) > 20)
		{
			Driver.errors = StringIntegerNode.addToFront(Driver.errors, "C4 Plastic Explosives", Integer.parseInt(bits.substring(65,70),2));
			String correction = "10100"; // Give Snake 20 explosives
			bits = bits.substring(0,60) + correction + bits.substring(65); 
		}
		
		head = StringIntegerNode.addToFront(head, "C4 Plastic Explosives", Integer.parseInt( bits.substring(60,65) ,2));
		
		
		// M79 Grenade Launcher (cannot be more than 90!)

		if (Integer.parseInt( bits.substring(85,87) + bits.substring(75,80), 2) > 90) 
		{
			Driver.errors = StringIntegerNode.addToFront(Driver.errors, "M79 Grenade Launcher", Integer.parseInt(bits.substring(65,70),2));
			String correction = "1011010"; // Give Snake 90 grenades
			bits = bits.substring(0,75) + correction.substring(2) + bits.substring(80,85) + correction.substring(0,2)
				   + bits.substring(87); 
		}
		
		head = StringIntegerNode.addToFront(head, "M79 Grenade Launcher", Integer.parseInt( bits.substring(85,87) + bits.substring(75,80) ,2));
		
		
		// Rations (max is 15)
		
		head = StringIntegerNode.addToFront(head, "Rations", Integer.parseInt( bits.substring(46,50) ,2));
		bit_pattern = bits;
		
		return head;
		
	}

}
