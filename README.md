# INTRODUCTION #
This is a password generator for Metal Gear, a video game for the Nintendo Entertainment System. 

In a strange case of irony, Metal Gear itself never appears in-game, although the game's title and Big Boss himself may say otherwise.

# HOW DOES THE GAME GENERATE A PASSWORD? #
I highly recommend you read Doug Babcock's guide here - http://www.dougbabcock.com/mg-passwords.txt


His FAQ does a fantastic job explaining how the password creation algorithm works. 

# HOW IS A PASSWORD "DECODED"? #
After the user inputs a 25 character password string into the textfield, my program decodes the given password (i.e. finds out what information is "encrypted" inside of it). A password keeps track
of a current "game-state", such as which bosses have been defeated, the equipment in Snake's inventory, which hostages have been rescued, etc. You can type a random password and the program will generate
the appropriate details that the password contains; for example, type a bunch of gibberish and see what the corresponding game-state will be!

However, all passwords must meet a set of conditions in order to be valid. If any one of these conditions is violated, the password will be invalid, and the game will reject it if you try to input it in the
password entry screen. Here are the conditions:

* The checksum digit (which is the last letter in the password) MUST be the correct letter

* Snake's rank must be at least 1 but no more than 4 stars

* The password cannot give Snake more than 20 mines

* The password cannot give Snake more than 20 C4 Explosives

* The password cannot give Snake more than 20 missiles

* The password can give Snake at most 90 grenades

* The password can give Snake at most 30 rockets

Here's an example - say you wanted to input the password "HELLO WORLD RADIO PHONE SPICY" (without the quotations, of course). The game is going to reject this password because the checksum digit is
incorrect and Snake's rank is 5; remember, the rank has to be at least 1 and no more than 4. My program corrects these errors so the password will become valid. Since Snake's rank is incorrect, my program
could change it to 1, but instead it changes the rank to 4 just to make the player's life a little easier.

The same idea holds for the ammunition contraints - if a password gives Snake 35 rockets, my program would correct the password by giving Snake 30 rockets instead of 0.

After making the appropriate corrections, the new password becomes "GELLO WORLD RADIO PHONE SPICN"; it's not quite as catchy as the one above, but at least the game will now accept this password.

# IMPROVEMENTS #
I've been thinking about adding sounds from the game to my program. For example, an alarm would go off if the user inputs an incorrect number of ammunition. A "validation" noise would play upon the creation
of a valid password, etc.

I could have done a better job with organizing my code; it's a little sloppy in some areas. The portion of code that determines which checkbox the user has checked off is admittedly pretty 
messy too - it's just a huge block of if/else/else if statements. A bug causes my code to incorrectly report the number of mines Snake currently has; the bug is especially odd because
it seems to appear at random. When I have time in the future, I will fix it. 

Also, whenever the checksum fails, my program reports the incorrect number of rounds the checksum is off by instead of the incorrect digit itself. If you look at the source code, you'll see why this happens.
If I have time in the future, I'll create a work-around for this issue.

Lastly, the menus themselves can be re-sized better.

# ACKNOWLEDGEMENTS #
Special thanks to 

* Doug Babcock for taking the time to write a detailed guide on how the password algorithm works. I referred to his guide many times.

* Patrick Lauke AKA redux for creating and uploading the in-game text font as a TFF file.

* Hideo Kojima for creating the Metal Gear video game series.

